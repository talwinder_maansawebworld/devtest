<?php 
require_once __DIR__.'/config.php';
Class Db extends Config{    
    protected $table = "info";
    function __construct(){ 
         parent::__construct();
    }
    
    function insertdb($record = array()){
        date_default_timezone_set("Asia/Kolkata");       
        
        $query ="INSERT INTO `info`(`image`, `canvas_data`) VALUES ('{$record['image']}','{$record['canvas_data']}')";
        $ins = mysqli_query($this->conn, $query);
        if($ins){           
            $info['type']='success';
            $info['message'] = "Hour logged successfully.";
            $info['id'] = mysqli_insert_id($this->conn); 
            echo json_encode($info);
            die();
        }else{
            $info['type']='danger';
            $info['message'] = "Something went wrong.Please try after some time.";
            echo json_encode($info);
            die();
        } 
    }
    
    function updateDb($record){
        $query ="UPDATE `info` SET `canvas_data`='{$record}' WHERE id = 25";
        $ins = mysqli_query($this->conn, $query);
    }
    
    function getData(){        
       
        $query ="SELECT *FROM apps_countries ORDER BY country_name";
        $result = mysqli_query($this->conn, $query); 
        while($row = $result->fetch_assoc()){
            $record[] = $row;
        } 
        return $record;
    }
    
    function getrecord(){
        $query ="SELECT *FROM {$this->table}";              
        $result = mysqli_query($this->conn, $query);
        $info =  mysqli_fetch_array($result, MYSQLI_ASSOC);        
        if($info){
            return $info['canvas_data'];
        }else{
            return 0;
        }
    }   
    
}
$main = new Db();